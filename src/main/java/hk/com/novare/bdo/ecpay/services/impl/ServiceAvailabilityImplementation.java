package hk.com.novare.bdo.ecpay.services.impl;

import hk.com.novare.bdo.ecpay.dto.requests.ServiceStatusRequest;
import hk.com.novare.bdo.ecpay.dto.responses.ServiceStatusReponse;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import hk.com.novare.bdo.ecpay.handlers.ErrorHandler;
import hk.com.novare.bdo.ecpay.services.ServiceAvailability;
import hk.com.novare.bdo.ecpay.utils.CipherService;
import hk.com.novare.bdo.ecpay.utils.FormatString;
import hk.com.novare.bdo.ecpay.utils.StatusEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;



@Service
public class ServiceAvailabilityImplementation implements ServiceAvailability {
	
    @Value("${mock.url.host}")
    private String host;

    private String mockUrl = "/5de7817d3700005400092975";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CipherService cipherService;

    @Autowired
    private FormatString formatString;

    private static final Logger logger = LogManager.getLogger(ServiceAvailabilityImplementation.class.getName());

    @Override
    public ResponseEntity<ServiceStatusReponse> serviceStatus(String token) throws CustomException {
        try {
        	//it should be merchant key
        	String merchantKey = "merchant key";
        	long epoch = System.currentTimeMillis()/1000;
        	String keyData = merchantKey + "|" + epoch;
            String keyEncrypted = cipherService.encrypt(keyData);
            
            ServiceStatusRequest request = new ServiceStatusRequest();
            request.setKey(keyEncrypted);
            String serviceText = request.toString();
            String encryptedRequest = cipherService.encrypt(serviceText);
            
            HttpHeaders header = new HttpHeaders();
            header.add("x-auth", token);
            logger.info("Check Service Status Request token {}", token);
            logger.info("Check Service Status Request object {}", request.toString());

            restTemplate.setErrorHandler(new ErrorHandler());
            ResponseEntity<ServiceStatusReponse> response = restTemplate.exchange(host + mockUrl, HttpMethod.POST, new HttpEntity<>(encryptedRequest, header), ServiceStatusReponse.class);
            logger.info("Check Service Status Response {}", response.getBody().toString());

            return new ResponseEntity<>(response.getBody(), response.getStatusCode());

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            logger.error("Check Service Status error {}", e);
            throw new CustomException(formatString.getExceptionMessageValue(e.getResponseBodyAsString()), e.getStatusCode(), e);
        } catch (Exception e) {
            logger.error("Check Service Status {}", e);
            throw new CustomException(StatusEnum.INTERNAL_SYSTEM_ERROR.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }
}
