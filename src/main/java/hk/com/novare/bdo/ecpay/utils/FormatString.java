package hk.com.novare.bdo.ecpay.utils;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import hk.com.novare.bdo.ecpay.entities.Status;

@Service
public class FormatString {
	
	public String getExceptionMessageValue (String exceptionMessage) {
		String message = exceptionMessage;
		if(exceptionMessage.contains("message")) message = new Gson().fromJson(exceptionMessage, Status.class).getMessage();
		return message;
	}
}
