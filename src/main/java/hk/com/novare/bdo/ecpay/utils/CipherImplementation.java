package hk.com.novare.bdo.ecpay.utils;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import hk.com.novare.bdo.ecpay.handlers.CustomException;

@Service
public class CipherImplementation implements CipherService {
	
		@Value("${cipher.key}")
		private String secretKey;
		
		private MessageDigest md;
	    private byte[] mdKey;
	    private byte[] keyBytes;
	    private SecretKey key;
	    private IvParameterSpec iv;
	    private Cipher cipher;
	    
	    private static final String BYTE_FORMAT = "utf-8";
	    private static final Logger logger = LogManager.getLogger(CipherImplementation.class.getName());
		
		public void initiateValues () throws Exception{
			 md = MessageDigest.getInstance("md5");
			 SimpleDateFormat df = new SimpleDateFormat("MMddyyyy");
		     String date = df.format(new Date());
			 String keyVal = secretKey + date;
			 mdKey = md.digest(keyVal.getBytes("utf-8"));
			 
			 keyBytes = Arrays.copyOf(mdKey, 24);
			 for (int j = 0, k = 16; j < 8;) {
	            keyBytes[k++] = keyBytes[j++];
			 }
			 
			 key = new SecretKeySpec(keyBytes, "DESede");
			 iv = new IvParameterSpec(new byte[8]);
			 cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		}
	
		@Override
		public String encrypt(String plainText) throws CustomException {
			try {
				logger.info("Encrypt Request {}", plainText);
				initiateValues();
		        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
		        String text = plainText;
		        byte[] dataInBytes = text.getBytes(BYTE_FORMAT);
		        byte[] cipherByte = cipher.doFinal(dataInBytes);
		        String cipherText = Base64.getMimeEncoder().encodeToString(cipherByte);
		        logger.info("Encrypt Response {}", cipherText);
				return cipherText;
			}catch (Exception e ) {
				logger.error("Encryption error {}", e);
				throw new CustomException("Encryption error", e);
			}
		}
	
		@Override
		public String decrypt(String encrypted) throws CustomException {
			try {
				logger.info("Decrypt Request {}", encrypted);
				initiateValues();
		        cipher.init(Cipher.DECRYPT_MODE, key, iv);
		        byte[] encData = Base64.getMimeDecoder().decode(encrypted);
		        byte[] cipherByte = cipher.doFinal(encData);
		        String cipherText = new String(cipherByte, BYTE_FORMAT);
		        logger.info("Decrypt Response {}", cipherText);
				return cipherText;
			}catch(Exception e) {
				logger.error("Decryption error {} ", e);
				throw new CustomException("Decryption error", e);
			}
		}
}
