package hk.com.novare.bdo.ecpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcpayApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcpayApplication.class, args);
	}

}
