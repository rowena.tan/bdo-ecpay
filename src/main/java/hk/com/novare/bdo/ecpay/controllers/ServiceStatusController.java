package hk.com.novare.bdo.ecpay.controllers;

import hk.com.novare.bdo.ecpay.dto.responses.ServiceStatusReponse;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import hk.com.novare.bdo.ecpay.services.ServiceAvailability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bdo/ecpay/service")
public class ServiceStatusController {

    @Autowired
    private ServiceAvailability serviceAvailability;
    
    @GetMapping("/status")
    public ResponseEntity<ServiceStatusReponse> serviceStatus (@RequestHeader (name = "X-Auth", required = true) String token) throws CustomException {
        return serviceAvailability.serviceStatus(token);
    }
}
