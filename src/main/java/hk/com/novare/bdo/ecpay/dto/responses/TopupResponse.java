package hk.com.novare.bdo.ecpay.dto.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import hk.com.novare.bdo.ecpay.dto.commons.APIStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class TopupResponse extends APIStatus{

	private String TelcoTraceNo;
	private String Message;
	
	public TopupResponse() {
		
	}
	
	public TopupResponse(String telcoTraceNo, String message) {
		super();
		this.TelcoTraceNo = telcoTraceNo;
		this.Message = message;
	}
	
	public TopupResponse(String telcoTraceNo, String message, String statusMessage, Integer Code) {
		super(statusMessage, Code);
		this.TelcoTraceNo = telcoTraceNo;
		this.Message = message;
	}

	@Override
	public String toString() {
		return "TopupResponse [TelcoTraceNo=" + TelcoTraceNo + ", Message=" + Message + "]";
	}
}
