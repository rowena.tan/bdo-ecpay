package hk.com.novare.bdo.ecpay.dto.requests;

import java.math.BigDecimal;

import hk.com.novare.bdo.ecpay.entities.Topup;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopupRequest extends Topup{
	
	public TopupRequest () {
		
	}
	
	public TopupRequest (String telco, String accntNo, String externalTag, BigDecimal amount, String clientTraceNo) {
		super();
		this.Telco = telco;
		this.AccntNo = accntNo;
		this.ExternalTag = externalTag;
		this.Amount = amount;
		this.ClientTraceNo = clientTraceNo;
	}
	
	private String Telco;
	private String AccntNo;
	private String ExternalTag;
	private BigDecimal Amount;
	private String ClientTraceNo;
	
	@Override
	public String toString() {
		return "TopupRequest [Telco=" + Telco + ", AccntNo=" + AccntNo + ", ExternalTag=" + ExternalTag + ", Amount="
				+ Amount + ", ClientTraceNo=" + ClientTraceNo + "]";
	}
}
