package hk.com.novare.bdo.ecpay.utils;

import lombok.Getter;

@Getter
public enum StatusEnum {
	
	SUCCESS(200, "Successful"),
	INVALID_REQUEST(400, "Invalid Request."),
	INVALID_ACCT_NO(400, "Invalid acctNo."),
	INVALID_TELCO(400, "Invalid telco."),
	INVALID_EXT_TAG(400, "Invalid externalTag."),
	INVALID_CLIENT_TRACE_NO(400, "Invalid clientTraceNo."),
	DUPLICATE_CLIENT_TRACE_NO(400, "Duplicate clientTraceNo."),
	TRANSACTION_DUPLICATE(400, "Transaction detected as duplicate. Try again after 60 seconds."),
	TOPUP_FAILED(400, "Topup request failed."),
	ACCESS_DENIED(401, "Access denied."),
	INVALID_LOGIN(401, "Invalid login."),
	INVALID_CLIENT(401, "Invalid client."),
	ACCT_DEACTIVATED(401, "Topup is deactivated."),
	ACCT_DISABLED(401, "Topup is disabled."),
	INTERNAL_SYSTEM_ERROR(500, "Internal system error");
	
	private final String status;
	private final int code;
	
	private StatusEnum(int code, String status) {
		this.status = status;
		this.code = code;
	}
}
