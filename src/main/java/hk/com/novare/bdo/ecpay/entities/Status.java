package hk.com.novare.bdo.ecpay.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Status {
	
	private String telcoTraceNo;
	private String message;
	
	@Override
	public String toString() {
		return "Status [telcoTraceNo=" + telcoTraceNo + ", message=" + message + "]";
	}
}
