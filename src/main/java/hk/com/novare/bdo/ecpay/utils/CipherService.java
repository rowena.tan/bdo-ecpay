package hk.com.novare.bdo.ecpay.utils;

import hk.com.novare.bdo.ecpay.handlers.CustomException;

public interface CipherService {		
		public String encrypt (String plainText) throws CustomException;
		
		public String decrypt (String encrypted) throws CustomException;
}
