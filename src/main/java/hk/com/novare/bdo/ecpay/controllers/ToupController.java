package hk.com.novare.bdo.ecpay.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hk.com.novare.bdo.ecpay.dto.requests.TopupInquiryStatus;
import hk.com.novare.bdo.ecpay.dto.requests.TopupRequest;
import hk.com.novare.bdo.ecpay.dto.responses.TopupResponse;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import hk.com.novare.bdo.ecpay.services.TopupService;

@RestController
@RequestMapping ("/bdo/ecpay/topup")
public class ToupController {
		@Autowired
		private TopupService topupService;

		@PostMapping
		public ResponseEntity<TopupResponse> topup ( @RequestHeader (name = "X-Auth", required = true) String token,
						@RequestBody TopupRequest topupRequest) throws CustomException{
			return topupService.topup(token, topupRequest);
		}

		@PostMapping("/status")
		public ResponseEntity<TopupResponse> topupInquiry ( @RequestHeader (name = "X-Auth", required = true) String token,
						@RequestBody TopupInquiryStatus inquiryRequest) throws CustomException{
			return topupService.topupInquiry(token, inquiryRequest);
		}
}
