package hk.com.novare.bdo.ecpay.configs;

import java.util.Collections;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class MainConfig {
	
	@Bean
	public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
	}
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("hk.com.novare.bdo.ecpay.controllers"))              
          .paths(PathSelectors.any())                          
          .build();                                           
    }
	
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "BDO EcPay APIs", 
	      "", 
	      "API TOS", 
	      "Terms of service", 
	      new Contact("Novare Technologies", "https://www.novare.com.hk/", "marketing@novare.com.hk"), 
	      "License of API", "API license URL", Collections.emptyList());
	}
}
