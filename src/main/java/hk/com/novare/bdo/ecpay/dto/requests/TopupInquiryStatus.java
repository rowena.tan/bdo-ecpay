package hk.com.novare.bdo.ecpay.dto.requests;

import hk.com.novare.bdo.ecpay.entities.Topup;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopupInquiryStatus extends Topup{
	
	private String ClientTraceNo;
	
	public TopupInquiryStatus () {
		
	}
	
	public TopupInquiryStatus(String clientTraceNo) {
		super();
		this.ClientTraceNo = clientTraceNo;
	}

	@Override
	public String toString() {
		return "TopupInquiryStatus [ClientTraceNo=" + ClientTraceNo + "]";
	}
}
