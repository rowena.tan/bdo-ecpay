package hk.com.novare.bdo.ecpay.services.impl;

import hk.com.novare.bdo.ecpay.services.TopupService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import hk.com.novare.bdo.ecpay.dto.requests.TopupInquiryStatus;
import hk.com.novare.bdo.ecpay.dto.requests.TopupRequest;
import hk.com.novare.bdo.ecpay.dto.responses.TopupResponse;
import hk.com.novare.bdo.ecpay.entities.Topup;
import hk.com.novare.bdo.ecpay.entities.Status;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import hk.com.novare.bdo.ecpay.handlers.ErrorHandler;
import hk.com.novare.bdo.ecpay.utils.CipherService;
import hk.com.novare.bdo.ecpay.utils.FormatString;
import hk.com.novare.bdo.ecpay.utils.StatusEnum;

@Service
public class TopupImplementation implements TopupService {
	
    @Value("${mock.url.host}")
    private String host;

    private String mockUrl = "/5ddf61be310000cb723ae6ed";

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private CipherService cipherService;
    @Autowired
    private FormatString formatString;

    private static final Logger logger = LogManager.getLogger(TopupImplementation.class.getName());

    @Override
    public ResponseEntity<TopupResponse> topup(String token, TopupRequest request) throws CustomException {
        try {
        	//it should be merchant key
        	String merchantKey = "merchant key";
        	long epoch = System.currentTimeMillis()/1000;
        	String keyData = merchantKey + "|" + epoch;
            String keyEncrypted = cipherService.encrypt(keyData);
            
            Topup topup = (Topup) request;
            topup.setKey(keyEncrypted);
            String topupText = topup.toString();
            String encrypted = cipherService.encrypt(topupText);

            HttpHeaders header = new HttpHeaders();
            header.add("x-auth", token);

            logger.info("Topup Request token {}", token);
            logger.info("Topup Request object {}", request.toString());
            restTemplate.setErrorHandler(new ErrorHandler());
            ResponseEntity<Status> response = restTemplate.exchange(host + mockUrl, HttpMethod.POST, new HttpEntity<String>(encrypted, header), Status.class);
            logger.info("Topup Response {}", response.getBody().toString());

            return new ResponseEntity<>(new TopupResponse(
                    response.getBody().getTelcoTraceNo(),
                    response.getBody().getMessage()),
                    response.getStatusCode()
            );
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            logger.error("Topup error {}", e);
            throw new CustomException(formatString.getExceptionMessageValue(e.getResponseBodyAsString()), e.getStatusCode(), e);
        } catch (Exception e) {
            logger.error("Topup error {}", e);
            throw new CustomException(StatusEnum.INTERNAL_SYSTEM_ERROR.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    @Override
    public ResponseEntity<TopupResponse> topupInquiry(String token, TopupInquiryStatus request) throws CustomException {
        try {
        	//it should be merchant key
        	String merchantKey = "merchant key";
        	long epoch = System.currentTimeMillis()/1000;
        	String keyData = merchantKey + "|" + epoch;
            String keyEncrypted = cipherService.encrypt(keyData);
            
            Topup topup = (Topup) request;
            topup.setKey(keyEncrypted);
            String topupText = topup.toString();
            String encrypted = cipherService.encrypt(topupText);

            HttpHeaders header = new HttpHeaders();
            header.add("x-auth", token);

            logger.info("Topup Inquiry Request token {}", token);
            logger.info("Topup Inquiry Request object {}", request.toString());
            restTemplate.setErrorHandler(new ErrorHandler());
            ResponseEntity<Status> response = restTemplate.exchange(host + mockUrl, HttpMethod.POST, new HttpEntity<String>(encrypted, header), Status.class);
            logger.info("Topup Inquiry Response {}", response.getBody().toString());
            
            return new ResponseEntity<>(new TopupResponse(response.getBody().getTelcoTraceNo(),
                    response.getBody().getMessage()), response.getStatusCode());
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            logger.error("Topup Inquiry error {}", e);
            throw new CustomException(formatString.getExceptionMessageValue(e.getResponseBodyAsString()), e.getStatusCode(), e);
        } catch (Exception e) {
            logger.error("Topup Inquiry error {}", e);
            throw new CustomException(StatusEnum.INTERNAL_SYSTEM_ERROR.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }
}
