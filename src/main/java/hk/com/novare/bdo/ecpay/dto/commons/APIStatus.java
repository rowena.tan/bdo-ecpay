package hk.com.novare.bdo.ecpay.dto.commons;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIStatus {
	
	private String Description;
	private Integer Code;
	
	public APIStatus () {
		
	}
	
	public APIStatus(String Description, Integer Code) {
		super();
		this.Description = Description;
		this.Code = Code;
	}
	@Override
	public String toString() {
		return "APIStatus [Description=" + Description + ", Code=" + Code + "]";
	}
}
