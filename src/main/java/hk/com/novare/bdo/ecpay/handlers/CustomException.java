
package hk.com.novare.bdo.ecpay.handlers;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomException extends Exception {

	private static final long serialVersionUID = 5060273832901744344L;

	private String exceptionMessage;
	private HttpStatus http;
	
	public CustomException (String exceptionMessage, HttpStatus http, Throwable e) {
		super(exceptionMessage, e);
		this.exceptionMessage = exceptionMessage;
		this.http = http;
	}
	
	public CustomException (String exceptionMessage, Throwable e) {
		super(exceptionMessage, e);
	}
}
