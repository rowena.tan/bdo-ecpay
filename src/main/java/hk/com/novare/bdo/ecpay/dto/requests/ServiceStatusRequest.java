package hk.com.novare.bdo.ecpay.dto.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceStatusRequest {
    private String key;
    
    public ServiceStatusRequest() {}
    
    public ServiceStatusRequest(String key) {
        this.key = key;
    }
}
