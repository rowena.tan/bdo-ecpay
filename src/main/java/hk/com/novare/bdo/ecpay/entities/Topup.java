package hk.com.novare.bdo.ecpay.entities;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Topup {
	
	private String key;
	private String telco;
	private String accntNo;
	private String externalTag;
	private BigDecimal amount;
	private String clientTraceNo;
	
	public Topup () {
		
	}
	
	public Topup(String key, String telco, String accntNo, String externalTag, BigDecimal amount,
			String clientTraceNo) {
		super();
		this.key = key;
		this.telco = telco;
		this.accntNo = accntNo;
		this.externalTag = externalTag;
		this.amount = amount;
		this.clientTraceNo = clientTraceNo;
	}

	@Override
	public String toString() {
		return "{key=" + key + ", telco=" + telco + ", accntNo=" + accntNo + ", externalTag=" + externalTag
				+ ", amount=" + amount + ", clientTraceNo=" + clientTraceNo + "}";
	}
}
