package hk.com.novare.bdo.ecpay.services;

import hk.com.novare.bdo.ecpay.dto.responses.ServiceStatusReponse;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import org.springframework.http.ResponseEntity;

public interface ServiceAvailability {

    ResponseEntity<ServiceStatusReponse> serviceStatus(String token) throws CustomException;
}
