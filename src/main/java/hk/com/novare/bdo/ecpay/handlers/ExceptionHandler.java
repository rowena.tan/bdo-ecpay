package hk.com.novare.bdo.ecpay.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import hk.com.novare.bdo.ecpay.dto.commons.APIStatus;
import hk.com.novare.bdo.ecpay.utils.StatusEnum;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {
	
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {Exception.class})
	public ResponseEntity<APIStatus> handleMSException (Exception exception, WebRequest webRequest){
		return new ResponseEntity<>(new APIStatus(exception.getLocalizedMessage(), StatusEnum.INTERNAL_SYSTEM_ERROR.getCode()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {CustomException.class})
	public ResponseEntity<APIStatus> handleAPIException (CustomException exception, WebRequest webRequest){
		return new ResponseEntity<>(new APIStatus(exception.getExceptionMessage(), exception.getHttp().value()), exception.getHttp());
	}
}
