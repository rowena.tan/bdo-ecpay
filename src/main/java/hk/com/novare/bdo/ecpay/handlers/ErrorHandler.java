package hk.com.novare.bdo.ecpay.handlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResponseErrorHandler;

public class ErrorHandler implements ResponseErrorHandler {
	
	private static final Logger logger = LogManager.getLogger(ErrorHandler.class.getName());
	
	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		int rawStatusCode = response.getRawStatusCode();
		HttpStatus statusCode = HttpStatus.resolve(rawStatusCode);
		return (statusCode != null ? hasError(statusCode) : hasError(rawStatusCode));
	}
	
	protected boolean hasError(HttpStatus statusCode) {
		return statusCode.isError();
	}

	protected boolean hasError(int unknownStatusCode) {
		HttpStatus.Series series = HttpStatus.Series.resolve(unknownStatusCode);
		return (series == HttpStatus.Series.CLIENT_ERROR || series == HttpStatus.Series.SERVER_ERROR);
	}
	
	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		HttpStatus statusCode = HttpStatus.resolve(response.getRawStatusCode());
		if (statusCode == null) {
			String message = getErrorMessage(
					response.getRawStatusCode(), response.getStatusText(),
					getResponseBody(response), getCharset(response));
			try {
				throw new CustomException(message, response.getStatusCode(), null);
			} catch (CustomException e) {
				logger.error("Error handler {} ", e.getLocalizedMessage());
			}
		}
		try {
			handleError(response, statusCode);
		} catch (CustomException e) {
			logger.error("Error handler {} ", e.getLocalizedMessage());
		}
	}

	private String getErrorMessage(
			int rawStatusCode, String statusText, @Nullable byte[] responseBody, @Nullable Charset charset) {

		String preface = rawStatusCode + " " + statusText + ": ";
		if (ObjectUtils.isEmpty(responseBody)) {
			return preface + "[no body]";
		}

		charset = charset == null ? StandardCharsets.UTF_8 : charset;
		int maxChars = 200;

		if (responseBody.length < maxChars * 2) {
			return preface + "[" + new String(responseBody, charset) + "]";
		}

		try {
			Reader reader = new InputStreamReader(new ByteArrayInputStream(responseBody), charset);
			CharBuffer buffer = CharBuffer.allocate(maxChars);
			reader.read(buffer);
			reader.close();
			buffer.flip();
			return preface + "[" + buffer.toString() + "... (" + responseBody.length + " bytes)]";
		}
		catch (IOException ex) {
			throw new IllegalStateException(ex);
		}
	}

	protected void handleError(ClientHttpResponse response, HttpStatus statusCode) throws IOException, CustomException {
		String statusText = response.getStatusText();
		HttpHeaders headers = response.getHeaders();
		byte[] body = getResponseBody(response);
		Charset charset = getCharset(response);
		String message = getErrorMessage(statusCode.value(), statusText, body, charset);

		switch (statusCode.series()) {
			case CLIENT_ERROR:
				throw HttpClientErrorException.create(statusCode, statusText, headers, body, charset);
			case SERVER_ERROR:
				throw HttpServerErrorException.create(statusCode, statusText, headers, body, charset);
			default:
				throw new CustomException(message, response.getStatusCode(), null);
		}
	}
	
	protected byte[] getResponseBody(ClientHttpResponse response) {
		try {
			return FileCopyUtils.copyToByteArray(response.getBody());
		}
		catch (IOException e) {
			logger.error("Error handler {} ", e.getLocalizedMessage());
		}
		return new byte[0];
	}

	@Nullable
	protected Charset getCharset(ClientHttpResponse response) {
		HttpHeaders headers = response.getHeaders();
		MediaType contentType = headers.getContentType();
		return (contentType != null ? contentType.getCharset() : null);
	}
}
