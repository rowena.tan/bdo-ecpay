package hk.com.novare.bdo.ecpay.services;

import org.springframework.http.ResponseEntity;

import hk.com.novare.bdo.ecpay.dto.requests.TopupInquiryStatus;
import hk.com.novare.bdo.ecpay.dto.requests.TopupRequest;
import hk.com.novare.bdo.ecpay.dto.responses.TopupResponse;
import hk.com.novare.bdo.ecpay.handlers.CustomException;

public interface TopupService {
	
	public ResponseEntity<TopupResponse> topup (String token, TopupRequest request) throws CustomException;
	
	public ResponseEntity<TopupResponse> topupInquiry (String token, TopupInquiryStatus request) throws CustomException;
}
