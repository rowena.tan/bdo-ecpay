package hk.com.novare.bdo.ecpay.dto.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import hk.com.novare.bdo.ecpay.dto.commons.APIStatus;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class ServiceStatusReponse extends APIStatus{

    @JsonProperty("GlobeIsUp")
    private boolean globeIsUp;

    @JsonProperty("SunIsUp")
    private boolean sunIsUp;

    @JsonProperty("CignalIsUp")
    private boolean cignalIsUp;

    @JsonProperty("MeralcoIsUp")
    private boolean meralcoIsUp;

    @JsonProperty("KBOServiceIsUp")
    private boolean kBOServiceIsUp;

    @JsonProperty("SmartIsUp")
    private boolean smartIsUp;
    
    public ServiceStatusReponse() {

    }

    public ServiceStatusReponse(boolean globeIsUp, boolean sunIsUp, boolean cignalIsUp, boolean meralcoIsUp,
                                boolean kBOServiceIsUp, boolean smartIsUp) {
        this.globeIsUp = globeIsUp;
        this.sunIsUp = sunIsUp;
        this.cignalIsUp = cignalIsUp;
        this.meralcoIsUp = meralcoIsUp;
        this.kBOServiceIsUp = kBOServiceIsUp;
        this.smartIsUp = smartIsUp;
    }

    @Override
    public String toString() {
        return "ServiceStatusResponse [GlobeIsUp=" + globeIsUp + ", SunIsUp=" + sunIsUp +
                ", CignalIsUp=" + cignalIsUp + ", MeralcoIsUp=" + meralcoIsUp +
                ", KBOServiceIsUp=" + kBOServiceIsUp + ", SmartIsUp=" + smartIsUp + "]";
    }
}
