package hk.com.novare.bdo.ecpay;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import hk.com.novare.bdo.ecpay.entities.Topup;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import hk.com.novare.bdo.ecpay.utils.CipherService;

@SpringBootTest
public class CipherTests {
		@Autowired
		private CipherService cipherService;
		
		private Topup account;
		
		@BeforeEach
		public void init() {
			account = new Topup("s2sRequest", "SMART", "09281234567",  "W15", new BigDecimal(15000), "PartnerTest1");
		}
		
		@Test
		public void decryptStringTest () throws CustomException {
			SimpleDateFormat df = new SimpleDateFormat("MMddyyyy");
		    String date = df.format(new Date());
			String plainText = "merchant key" + "|" + date;
			String encrypted = cipherService.encrypt(plainText);
			String decrypted = cipherService.decrypt(encrypted);
			assertEquals(plainText, decrypted);
		}
		
		@Test
		public void decryptObjectTest () throws CustomException {
			String plainText = account.toString();
			String encrypted = cipherService.encrypt(plainText);
			String decrypted = cipherService.decrypt(encrypted);
			Map<String, String> acctMap = toMap(decrypted);
			Topup acctResponse = new Topup();
			acctResponse.setKey(acctMap.get("key"));
			acctResponse.setTelco(acctMap.get("telco"));
			acctResponse.setAccntNo(acctMap.get("accntNo"));
			acctResponse.setExternalTag(acctMap.get("externalTag"));
			acctResponse.setAmount(new BigDecimal(Double.parseDouble(acctMap.get("amount"))));
			acctResponse.setClientTraceNo(acctMap.get("clientTraceNo"));
			assertThat(account).isEqualToComparingFieldByField(acctResponse);
		}
		
		private Map<String, String> toMap (String json){
			json = json.substring(1,json.length()-1);
	        String[] jsonArr = json.split(",");
	        Map<String, String> jsonMap = new HashMap<String, String>();
	        for (int i=0; i<jsonArr.length; i++) {
	            String pair = jsonArr[i].trim();
	            if(pair.contains("key")) {
	            	String[] keyPair = pair.split("key");
	            	jsonMap.put("key", keyPair[1].substring(1, keyPair[1].length()));
	            }else {
	            	String[] keyValue = pair.split("=");
	            	jsonMap.put(keyValue[0], keyValue[1]);
	            }
	        }
	        return jsonMap;
		}
}
