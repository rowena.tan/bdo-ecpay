package hk.com.novare.bdo.ecpay;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import hk.com.novare.bdo.ecpay.controllers.ToupController;
import hk.com.novare.bdo.ecpay.dto.requests.TopupInquiryStatus;
import hk.com.novare.bdo.ecpay.dto.requests.TopupRequest;
import hk.com.novare.bdo.ecpay.dto.responses.TopupResponse;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import hk.com.novare.bdo.ecpay.services.TopupService;

@SpringBootTest
public class TopupControllerTests {
	
		@Autowired
		private ToupController topupController;
		@Autowired
		private TopupService service ;
		
		private TopupRequest topupRequest;
		private TopupInquiryStatus inquiryRequest;
		private String header = "bh1212423asdasd";

		@BeforeEach
		public void setAccountRequest() {
			topupRequest = new TopupRequest("SMART", "09281234567", "W15", new BigDecimal(15000), "PartnerTest1");
		}
		
		@BeforeEach
		public void setInquiryRequest() {
			inquiryRequest = new TopupInquiryStatus("PartnerTest1");
		}
		
		//topup
		@Test
		public void topup_expectedSuccessfulResponseTest () throws CustomException {
			ReflectionTestUtils.setField(service, "mockUrl", "/5ddf61be310000cb723ae6ed");
			ResponseEntity<TopupResponse> response = topupController.topup(header, topupRequest);
			assertThat(response.getBody()).isEqualToComparingFieldByField(new TopupResponse("9286291", "Successful"));
		}
		
		@Test
		public void topup_expectedInvalidResponseTest () throws CustomException {
			Assertions.assertThrows(CustomException.class, () -> { 
				ReflectionTestUtils.setField(service, "mockUrl", "/5ddf629a3100006bce3ae6f0");
				ResponseEntity<TopupResponse> response  = topupController.topup(header, topupRequest);
				assertEquals("Invalid request.", response.getBody().getDescription());
				assertEquals(400,response.getBody().getCode());
				assertEquals(400, response.getStatusCode().value());
			});
		}
		
		@Test
		public void topup_expectedAccessDeniedResponseTest () throws CustomException {
			Assertions.assertThrows(CustomException.class, () -> { 
				ReflectionTestUtils.setField(service, "mockUrl", "/5ddf6523310000526c3ae70b");
				ResponseEntity<TopupResponse> response  = topupController.topup(header, topupRequest);
				assertEquals("Access denied.", response.getBody().getDescription());
				assertEquals(401, response.getBody().getCode() );
				assertEquals(401, response.getStatusCode().value());
			});
		}
		
		@Test
		public void topup_expectedSystemErrorResponseTest () throws CustomException {
			Assertions.assertThrows(CustomException.class, () -> { 
				ReflectionTestUtils.setField(service, "mockUrl", "/5ddf6554310000526c3ae70c");
				ResponseEntity<TopupResponse> response  = topupController.topup(header, topupRequest);
				assertEquals("Internal system error", response.getBody().getDescription());
				assertEquals(500, response.getBody().getCode());
				assertEquals(500, response.getStatusCode().value());
			});
		}
		
		
		@Test
		public void topup_expectedDuplicateErrorTest () throws CustomException {
			Assertions.assertThrows(CustomException.class, () -> { 
				ReflectionTestUtils.setField(service, "mockUrl", "/5de3edf23000005b009f78d3");
				ResponseEntity<TopupResponse> response  = topupController.topup(header, topupRequest);
				assertEquals("Duplicate clientTraceNo.", response.getBody().getDescription());
				assertEquals(500, response.getBody().getCode());
				assertEquals(500, response.getStatusCode().value());
			});
		}
		
		//inquire status
		@Test
		public void inquireStatus_expectedSuccessfulResponseTest () throws CustomException {
			ReflectionTestUtils.setField(service, "mockUrl", "/5ddf61be310000cb723ae6ed");
			ResponseEntity<TopupResponse> response = topupController.topupInquiry(header, inquiryRequest);
			assertThat(response.getBody()).isEqualToComparingFieldByField( new TopupResponse("9286291", "Successful"));
		}
		
		@Test
		public void inquireStatus_expectedInvalidResponseTest () {
			Assertions.assertThrows(CustomException.class, () -> { 
				ReflectionTestUtils.setField(service, "mockUrl", "/5ddf629a3100006bce3ae6f0");
				ResponseEntity<TopupResponse> response  = topupController.topupInquiry(header, inquiryRequest);
				assertEquals("Invalid request.", response.getBody().getDescription());
				assertEquals(400,response.getBody().getCode());
				assertEquals(400, response.getStatusCode().value());
			});
		}
		
		@Test
		public void inquireStatus_expectedAccessDeniedResponseTest () {
			Assertions.assertThrows(CustomException.class, () -> { 
				ReflectionTestUtils.setField(service, "mockUrl", "/5ddf6523310000526c3ae70b");
				ResponseEntity<TopupResponse> response  = topupController.topupInquiry(header, inquiryRequest);
				assertEquals("Access denied.", response.getBody().getDescription());
				assertEquals(401, response.getBody().getCode() );
				assertEquals(401, response.getStatusCode().value());
			});
		}
		
		@Test
		public void inquireStatus_expectedSystemErrorResponseTest () {
			Assertions.assertThrows(CustomException.class, () -> { 
				ReflectionTestUtils.setField(service, "mockUrl", "/5ddf6554310000526c3ae70c");
				ResponseEntity<TopupResponse> response  = topupController.topupInquiry(header, inquiryRequest);
				assertEquals("Internal system error", response.getBody().getDescription());
				assertEquals(500, response.getBody().getCode());
				assertEquals(500, response.getStatusCode().value());
			});
		}
}
