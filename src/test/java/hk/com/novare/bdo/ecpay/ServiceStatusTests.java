package hk.com.novare.bdo.ecpay;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Objects;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import hk.com.novare.bdo.ecpay.controllers.ServiceStatusController;
import hk.com.novare.bdo.ecpay.dto.responses.ServiceStatusReponse;
import hk.com.novare.bdo.ecpay.handlers.CustomException;
import hk.com.novare.bdo.ecpay.services.ServiceAvailability;

@SpringBootTest
class ServiceStatusTests {

    @Autowired
    private ServiceStatusController controller;

    @Autowired
    ServiceAvailability serviceAvailability;

    private String token = "JAsldkja11aaAXsaaw123kjlsdiasd";

    @Test
    public void checkServiceStatus_expectedSuccessfulResponseTest() throws CustomException {
        ReflectionTestUtils.setField(serviceAvailability, "mockUrl", "/5de7817d3700005400092975");
        ResponseEntity<ServiceStatusReponse> response = controller.serviceStatus(token);
        assertThat(response.getBody()).isEqualToComparingFieldByField(new ServiceStatusReponse(
                true,
                true,
                true,
                true,
                true,
                true
        ));
    }

    @Test
    void checkServiceStatus_expectedInvalidResponse() throws CustomException {
        Assertions.assertThrows(CustomException.class, () -> {
           ReflectionTestUtils.setField(serviceAvailability, "mockUrl", "/5ddf629a3100006bce3ae6f0");
           ResponseEntity<ServiceStatusReponse> response = controller.serviceStatus(token);
           assertEquals("Invalid request.", Objects.requireNonNull(response.getBody()).getDescription());
           assertEquals(400, response.getBody().getCode());
           assertEquals(400, response.getStatusCode().value());
        });
    }

    @Test
    public void checkServiceStatus_expectedAccessDeniedResponseTest () throws CustomException {
        Assertions.assertThrows(CustomException.class, () -> {
            ReflectionTestUtils.setField(serviceAvailability, "mockUrl", "/5ddf6523310000526c3ae70b");
            ResponseEntity<ServiceStatusReponse> response  = controller.serviceStatus(token);
            assertEquals("Access denied.", Objects.requireNonNull(response.getBody()).getDescription());
            assertEquals(401, response.getBody().getCode() );
            assertEquals(401, response.getStatusCode().value());
        });
    }

    @Test
    public void checkServiceStatus_expectedSystemErrorResponseTest () throws CustomException {
        Assertions.assertThrows(CustomException.class, () -> {
            ReflectionTestUtils.setField(serviceAvailability, "mockUrl", "/5ddf6554310000526c3ae70c");
            ResponseEntity<ServiceStatusReponse> response  = controller.serviceStatus(token);
            assertEquals("Internal system error", Objects.requireNonNull(response.getBody()).getDescription());
            assertEquals(500, response.getBody().getCode());
            assertEquals(500, response.getStatusCode().value());
        });
    }


    @Test
    public void checkServiceStatus_expectedDuplicateErrorTest () throws CustomException {
        Assertions.assertThrows(CustomException.class, () -> {
            ReflectionTestUtils.setField(serviceAvailability, "mockUrl", "/5de3edf23000005b009f78d3");
            ResponseEntity<ServiceStatusReponse> response  = controller.serviceStatus(token);
            assertEquals("Duplicate clientTraceNo.", Objects.requireNonNull(response.getBody()).getDescription());
            assertEquals(500, response.getBody().getCode());
            assertEquals(500, response.getStatusCode().value());
        });
    }
}
